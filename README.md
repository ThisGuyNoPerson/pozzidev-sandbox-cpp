## Used Libraries
- [SDL2](https://www.libsdl.org/)

Their installation occurs automatically during project compilation.


## Build Instruction
Install the following tools:
- Cmake.
- Meson.
- Makefile.
- Msys2.
- Mingw via Msys2.

Add `C:\msys64\ucrt64\bin` directory to `PATH`.
Open the project directory `pozzidev-sandbox-cpp` and run the following commands:
- `meson setup build` Wait for the command to complete. This may take several minutes.
- `make all` Wait for the build to complete. This may take several minutes. Once completed, the compiled file will run automatically. Compiled file location along path `\build\PozziDev Sandbox.exe`.

Recompilation of a file is done in the same way: `make all`.
You can also run the application without recompilation using the `make start` command.