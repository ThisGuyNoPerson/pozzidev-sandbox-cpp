start:
	'./build/PozziDev Sandbox.exe'
	strip './build/PozziDev Sandbox.exe'

all:
	meson compile -C build
	make start