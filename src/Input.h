/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "SDL.h"

extern bool mouse_button1_pressed;
extern bool mouse_button2_pressed;
extern bool control_key_pressed;

void mouse_button_down(SDL_Event event);
void mouse_button_up(SDL_Event event);
void key_down(SDL_Event event);
void key_up(SDL_Event event);
void scroll_action(SDL_Event event);