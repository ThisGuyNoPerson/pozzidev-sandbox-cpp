/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Input.h"
#include "Brush.h"

bool mouse_button1_pressed = false;
bool mouse_button2_pressed = false;
bool control_key_pressed = false;

void mouse_button_down(SDL_Event event) {
    if(event.button.button == SDL_BUTTON_LEFT)
        mouse_button1_pressed = true;
    if(event.button.button == SDL_BUTTON_RIGHT)
        mouse_button2_pressed = true;
}

void mouse_button_up(SDL_Event event) {
    if(event.button.button == SDL_BUTTON_LEFT)
        mouse_button1_pressed = false;
    if(event.button.button == SDL_BUTTON_RIGHT)
        mouse_button2_pressed = false;
}

void key_down(SDL_Event event) {
    if(event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL)
        control_key_pressed = true;
}

void key_up(SDL_Event event) {
    if(event.key.keysym.sym == SDLK_LCTRL || event.key.keysym.sym == SDLK_RCTRL)
        control_key_pressed = false;
}

void scroll_action(SDL_Event event) {
    int new_brush_radius = brush_radius;
    if(event.wheel.preciseY > 0) {
        new_brush_radius += event.wheel.preciseY;
    }else if(event.wheel.preciseY < 0) {
        new_brush_radius += event.wheel.preciseY;
    }
    if(new_brush_radius < 1)
        new_brush_radius = 1;
    if(new_brush_radius > 65)
        new_brush_radius = 65;
    brush_radius = new_brush_radius;
}