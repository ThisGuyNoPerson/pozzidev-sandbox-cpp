/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SIMULATION_H
#define SIMULATION_H

#include <iostream>
#include <stdio.h>

#include "Elements.h"

using namespace std;

typedef long long int64;
typedef unsigned long long uint64;

struct point {
    float x = 0;
    float y = 0;
};

struct part {
    int id = AIR;
    point velocity;
    bool updated = false;
};

constexpr int PMAPW = 340;
constexpr int PMAPH = 340-50;
extern part PMAP[][PMAPH];

void clear_sim();
void create_pixel(int x, int y, int pixel);
void create_line(int x1, int y1, int x2, int y2, int pixel);
void default_powder_update(int x, int y);
void update();
bool is_bordered(int x, int y);

#endif