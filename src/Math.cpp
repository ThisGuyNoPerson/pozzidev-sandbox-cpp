/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "Math.h"
#include "Simulation.h"

float lerp(float a, float b, float t) {
    return a + t * (b - a);
}

float clamp(float x, float mn, float mx) {
    return std::max(std::min(x, mx), mn);
}

void mouse_map_position_limit(int& px, int& py) {
    int padding = 5;
    if(px < padding)
        px = padding;
    if(px > PMAPW - padding)
        px = PMAPW - padding;
    if(py < padding)
        py = padding;
    if(py > PMAPH - padding)
        py = PMAPH - padding;
}
