/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "PozziDevSandbox.h"
#include "Simulation.h"
#include "Math.h"
#include "SDL.h"

int BMAP[PMAPW][PMAPH];
int brush_radius = 22;
int current_part_id = STNE;

void brush_update() {
    for (int x = 0; x < PMAPW; x++)
    {
        for (int y = 0; y < PMAPH; y++)
        {
            BMAP[x][y] = 0;
        }
    }
    for (int i = 0; i < 1000; i++)
    {
        int mx = round(mouse_x / 3);
        int my = round(mouse_y / 3);
        mouse_map_position_limit(mx, my);
        int px = round((brush_radius - 1) * sin(i * PI / 360));
        int py = round((brush_radius - 1) * cos(i * PI / 360));
        int bx = (int)(mx + px);
        int by = (int)(my + py);
        mouse_map_position_limit(bx, by);
        if(!is_bordered(bx, by))
            BMAP[bx][by] = 1;
    }
}

void brush_draw(int x, int y, int id) {
    for (int cx = 0; cx < brush_radius * 2; cx++)
    {
        for (int cy = 0; cy < brush_radius * 2; cy++)
        {
            int bpx = x + (cx - brush_radius);
            int bpy = y + (cy - brush_radius);
            int a = (cx - brush_radius) * (cx - brush_radius);
            int b = (cy - brush_radius) * (cy - brush_radius);
            bool inside = (a + b) < brush_radius * brush_radius;
            if(inside)
                create_pixel(bpx, bpy, id);
        }
    }
}

void brush_draw_line(int x1, int y1, int x2, int y2, int id) {
    for (float i = 0; i < 1; i+=.005f)
    {
        int lx = lerp(x1, x2, i);
        int ly = lerp(y1, y2, i);
        brush_draw(lx, ly, id);
    }
}