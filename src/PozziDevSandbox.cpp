/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "PozziDevSandbox.h"
#include "Simulation.h"
#include "Renderer.h"
#include "Input.h"
#include "Math.h"
#include "Brush.h"
#include "WindowIcon.h"

SDL_Window *window;
SDL_Renderer *renderer;

int window_width = PMAPW * 3;
int window_height = PMAPH * 3;

int mouse_x = 0;
int mouse_y = 0;
int old_mouse_x = 0;
int old_mouse_y = 0;

int init_app() {

    clear_sim();

    create_line(10, 100, 100, 100, 2);
    create_line(40, 200, 130, 250, 2);

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", ("An error occurred while executing the program."), NULL);
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Critical error occured: %s", SDL_GetError());
        return -1;
    }

    if (SDL_CreateWindowAndRenderer(window_width, window_height, SDL_WINDOW_RESIZABLE, &window, &renderer) < 0) {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", ("An error occurred while executing the program."), NULL);
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Critical error occured: %s", SDL_GetError());
        return -2;
    }

    SDL_SetWindowResizable(window, SDL_FALSE);
    SDL_SetWindowTitle(window, "PozziDev Sandbox");

    SDL_Surface *surface = SDL_GetWindowSurface(window);
    surface->w = 32;
    surface->h = 32;
    surface->pixels = dunes;
    SDL_SetWindowIcon(window, surface);

    SDL_Event event;
    while (true) {
        Uint32 start_timing = SDL_GetTicks();
        SDL_GetMouseState(&mouse_x, &mouse_y);
        int px = SDL_round(mouse_x / 3);
        int py = SDL_round(mouse_y / 3);
        SDL_PollEvent(&event);
        if (event.type == SDL_QUIT) {
            break;
        }else if (event.type == SDL_MOUSEBUTTONDOWN) {
            mouse_button_down(event);
        }else if (event.type == SDL_MOUSEBUTTONUP) {
            mouse_button_up(event);
        }else if (event.type == SDL_KEYDOWN) {
            key_down(event);
        }else if (event.type == SDL_KEYUP) {
            key_up(event);
        }else if (event.type == SDL_MOUSEWHEEL) {
            scroll_action(event);
        }
        if(mouse_button1_pressed && !control_key_pressed) {
            mouse_map_position_limit(px, py);
            brush_draw_line(old_mouse_x, old_mouse_y, px, py, 1);
        }
        brush_update();
        update();
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        render();
        SDL_RenderPresent(renderer);
        Uint32 end_timing = SDL_GetTicks();
        float secondsElapsed = (end_timing - start_timing) / 1000.0f;
        SDL_Delay(10);
        old_mouse_x = px;
        old_mouse_y = py;
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return 0;
}