/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <string>
#include <stdio.h>

#include "Simulation.h"
#include "Renderer.h"
#include "PozziDevSandbox.h"
#include "Elements.h"
#include "Brush.h"

using namespace std;

int64 frame = 0;

SDL_Color make_SDL_Color(int r, int g, int b, int a) {
    SDL_Color color;
    color.r = r;
    color.g = g;
    color.b = b;
    color.a = a;
    return color;
}

SDL_Color get_part_color(int id) {
    switch (id)
    {
        case SAND:
        return make_SDL_Color(230, 186, 92, 255);
        case STNE:
        return make_SDL_Color(90, 90, 90, 255);
    }
}

void render() {

    SDL_FRect rect;

    /* Render map */
    for (int x = 0; x < PMAPW; x++)
    {
        for (int y = 0; y < PMAPH; y++)
        {
            int pixel = PMAP[x][y].id;
            if(pixel != AIR) {
                rect.w = 3;
                rect.x = x * rect.w;
                rect.y = y * rect.w;
                rect.h = rect.w;
                SDL_Color color = get_part_color(pixel);
                SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
                SDL_RenderFillRectF(renderer, &rect);
            }
        }
    }

    /* Render brush */
    for (int x = 0; x < PMAPW; x++)
    {
        for (int y = 0; y < PMAPH; y++)
        {
            int pixel = BMAP[x][y];
            if(pixel != 0) {
                rect.w = 3;
                rect.x = x * rect.w;
                rect.y = y * rect.w;
                rect.h = rect.w;
                SDL_Color color = make_SDL_Color(200, 200, 200, 255);
                SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
                SDL_RenderFillRectF(renderer, &rect);
            }
        }
    }
    
    frame++;

}