/*

PozziDev Sandbox
Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <cstring>

#include "Simulation.h"
#include "Math.h"
#include "Elements.h"
#include "Elements/SAND.h"
#include "SDL.h"

part PMAP[PMAPW][PMAPH];

part get_part(int x, int y) {
    return PMAP[x][y];
}

part air_part() {
    part p;
    p.id = AIR;
    p.velocity.x = 0;
    p.velocity.y = 0;
    return p;
}

void clear_sim() {
    for (int x = 0; x < PMAPW; x++) {
        for (int y = PMAPH; y > 0; y--) {
            PMAP[x][y] = air_part();
        }
    }
}

void place_part(int x, int y, part p) {
    PMAP[x][y] = p;
}

bool is_bordered(int x, int y) {
    if(x < 5 || y < 5 || x > PMAPW-5 || y > PMAPH-5)
        return true;
    return false;
}

void create_pixel(int x, int y, int pixel) {
    if(is_bordered(x, y))
        return;
    if(PMAP[x][y].id != AIR)
        return;
    PMAP[x][y].id = pixel;
}

void create_line(int x1, int y1, int x2, int y2, int pixel) {
    for (float i = 0; i < 1; i+=.001f)
    {
        int lx = lerp(x1, x2, i);
        int ly = lerp(y1, y2, i);
        create_pixel(lx, ly, pixel);
    }
}

point collide(float x1, float y1, float x2, float y2) {
    point p;
    p.x = x2;
    p.y = y2;
    for (float ry = 0; ry < ((int)y2 - (int)y1); ry++)
    {
        if(get_part(p.x, (int)y1 + ry + 1).id != AIR){
            p.y = y1 + ry;
            return p;
        }
    }
    return p;
}

void default_powder_update(int x, int y) {
    part cp = get_part(x, y);
    if(!cp.updated) {
        cp.updated = true;
        bool move_side = false;
        if(PMAP[x][y + 1].id != AIR) {
            int rnd = rand() % 2;
            if(PMAP[x + 1][y + 1].id == AIR && PMAP[x + 1][y].id == AIR && rnd == 1) {
                cp.velocity.x += 1;
                move_side = true;
            }else if(PMAP[x - 1][y + 1].id == AIR && PMAP[x - 1][y].id == AIR && rnd != 1) {
                cp.velocity.x -= 1;
                move_side = true;
            }
        }else {
            cp.velocity.y = SDL_clamp(cp.velocity.y + (2 * 0.5001f), -15.f, 15.f);
        }
        float vx = x + cp.velocity.x; 
        float vy = y + cp.velocity.y;
        if (is_bordered(vx, vy)){
            place_part(x, y, air_part());
            return;
        }
        int svy = (int)vy;
        point p = collide(x, y, vx, vy);
        vx = p.x;
        vy = p.y;
        if((int)vy != (int)svy)
            cp.velocity.y = 0;
        if(move_side)
            cp.velocity.x = 0;
        part temp = get_part((int)vx, (int)vy);
        place_part((int)vx, (int)vy, cp);
        place_part((int)x, (int)y, temp);
    }
}

void update() {
    for (int x = 0; x < PMAPW; x++)
    {
        for (int y = 0; y < PMAPH; y++)
        {
            PMAP[x][y].updated = false;
        }
    }
    for (int x = 0; x < PMAPW; x++)
    {
        for (int y = 0; y < PMAPH; y++)
        {
            int pixel = PMAP[x][y].id;
            switch (pixel) {
                case SAND:
                SAND_update(x, y);
                break;
            }
        }
    }
}