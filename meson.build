# PozziDev Sandbox
# Copyright (C) 2023 ThisGuyNoPerson <thisguynoperson@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

project(
	'pozzidev-sandbox',
	[
        'cpp',
        'c',
    ],
)

debug = true

sdl2_dep = dependency(
    'sdl2',
    static: true,
    method: 'pkg-config',
    required: true
)

dependencies = [
    sdl2_dep,
]

sources = [
    'src/main.cpp',
    'src/PozziDevSandbox.cpp',
    'src/Simulation.cpp',
    'src/Renderer.cpp',
    'src/Input.cpp',
    'src/Math.cpp',
    'src/Brush.cpp',
    'src/WindowIcon.cpp',
    'src/Elements/SAND.cpp',
]

link_args = [
    '-static',
    '-Wl,--no-undefined',
    '-Wl,--allow-multiple-definition',
    '-Wunused-variable',
]

executable(
    'PozziDev Sandbox',
    sources,
    win_subsystem: debug ? 'console' : 'windows',
    link_args: link_args,
    dependencies: dependencies,
)